# Nintendo Wii GOG Galaxy 2.0 Integration

A GOG Galaxy 2.0 integration with the Dolphin emulator.
This is a fork of AHCoder (on GitHub)'s PCSX2 GOG Galaxy Plugin edited for support with Dolphin! 
If you want to download it go here:

Thank you AHCoder for the original program, and the Index file 
which gives the game database is from GameTDB.

Setup:
Just download the file here and copy and paste it into:
C:\Users\coolj\AppData\Local\GOG.com\Galaxy\plugins\installed

Open up user_config.py and edit the ROM and Dolphin location
Make sure you use "/" instead of "\" for the file paths
Go into GOG Galaxy 2.0, click on integrations and connect the one with "Nintendo Wii" 
and you're done.

Limitations:
All ROMs must be in the same folder, no subfolders, and also the name of the ROM must be equivalent
to its counterpart in Index.txt. You can look up the name in Index.txt and edit it
accordingly.
